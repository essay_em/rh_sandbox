<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE book [
<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">
<!ENTITY % DOCBOOK_ENTS PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/dbcentx.mod">
%DOCBOOK_ENTS;
]>
<?asciidoc-toc maxdepth="3"?><?asciidoc-numbered?><book xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" version="5.0">
<info>
<title>DocBook to AsciiDoc conversions and CCutil cleanup</title><subtitle>
  Cleaning up AsciiDoc files after using doctoascii DocBook XML conversion utility
</subtitle>

<date>2019-06-07</date>
<productname>
  Doctoascii conversion utility
</productname>
<productnumber>
  0.1
</productnumber>
<release>
  0.1
</release>
<abstract>
 <para>
   This guide describes some essential clean-up you must perform after a DocBook XML file is converted to AsciiDoc using the `doctoascii` utility.
    </para>
</abstract>
<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Author_Group.xml"/>
<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Common_Content/Legal_Notice.xml"/>
<orgname>Red Hat</orgname>
</info>
<chapter xml:id="ccutil_intro">
<title>DocBook to AsciiDoc conversions and CCutil cleanup</title>
<simpara>After you use the <literal>doctoascii</literal> conversion utility to convert DocBook (XML) files to AsciiDoc, you must do some additional manual cleanup so that the files will process through CCutil.</simpara>
<simpara>The tasks include:</simpara>
<itemizedlist>
<listitem>
<simpara><xref linkend="cleaning_up_image_tags"/></simpara>
</listitem>
<listitem>
<simpara><xref linkend="cleaning_up_image_file_references"/></simpara>
</listitem>
<listitem>
<simpara><xref linkend="cleaning_up_variable_references_in_a_topic"/></simpara>
</listitem>
<listitem>
<simpara><xref linkend="verifying_that_asciidoc_variables_are_defined"/></simpara>
</listitem>
<listitem>
<simpara><xref linkend="troubleshooting_when_variables_don_t_expand"/></simpara>
</listitem>
<listitem>
<simpara><xref linkend="troubleshooting_when_changes_don_t_appear_in_the_output_after_you_modified_the_input_file"/></simpara>
</listitem>
<listitem>
<simpara><xref linkend="adjusting_heading_levels_in_the_literal_master_adoc_literal_file"/></simpara>
</listitem>
</itemizedlist>
<formalpara>
<title>Prerequisites</title>
<para>The following prerequisites apply:</para>
</formalpara>
<itemizedlist>
<listitem>
<simpara>You are using the the <literal>doctoascii</literal> conversion utility and understand its basic operation.</simpara>
</listitem>
<listitem>
<simpara>You are familiar with AsciiDoc syntax and tagging.</simpara>
</listitem>
<listitem>
<simpara>You have some knowledge of XML or DocBook tagging.</simpara>
</listitem>
</itemizedlist>
<section xml:id="conv-cleanup">
<title>Image tag conversion cleanup</title>
<simpara>Change the converted <literal>doctoascii</literal> image tag to an AsciiDoc-friendly tag.</simpara>
<simpara>The XML image tag, including the figure title, is converted by <literal>doctoascii</literal>. When the converted tag contains a figure caption, it is not processed as expected when run through CCutil.</simpara>
<simpara>The following examples show the pre- and post-conversion image tags that contain a figure caption.</simpara>
<formalpara>
<title>Pre-conversion XML example</title>
<para>
<screen>&lt;figure id="fig.prod-renewal"&gt;
  &lt;title&gt;(Expired) Product Renewal Information&lt;/title&gt;
    &lt;mediaobject&gt;
      &lt;imageobject&gt;
        &lt;imagedata fileref="images/prod-renewal.png" /&gt;
      &lt;/imageobject&gt;
    &lt;/mediaobject&gt;
&lt;/figure&gt;</screen>
</para>
</formalpara>
<formalpara>
<title>Post-conversion AsciiDoc example</title>
<para>
<screen>image::images/prod-renewal.png[(Expired) Product Renewal Information]</screen>
</para>
</formalpara>
</section>
<section xml:id="cleaning_up_image_tags" remap="_cleaning_up_image_tags">
<title>Cleaning up image tags</title>
<simpara>CCutil fails when the image tag includes text inside square brackets. You must remove the text within the brackets and, if you want the figure to have a title, place the text above the image tag.</simpara>
<orderedlist numeration="arabic">
<title>Procedure</title>
<listitem>
<simpara>Delete, or cut, the text inside the square brackets while leaving the brackets.</simpara>
<screen>image::images/prod-renewal.png[(Expired) Product Renewal Information]
                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^</screen>
<formalpara>
<title>Result</title>
<para>
<screen>image::images/prod-renewal.png[]</screen>
</para>
</formalpara>
</listitem>
<listitem>
<simpara>(Optional) If you want an image caption, insert it above the image tag. Don’t forget the “.” (period).</simpara>
<formalpara>
<title>Result</title>
<para>
<screen>.(Expired) Product Renewal Information
image::images/prod-renewal.png[]</screen>
</para>
</formalpara>
</listitem>
</orderedlist>
</section>
<section xml:id="cleaning_up_image_file_references" remap="_cleaning_up_image_file_references">
<title>Cleaning up image file references</title>
<simpara>Update the image tag if <literal>master.adoc</literal> identifies the image directory location.</simpara>
<simpara>The <literal>master.adoc</literal> file frequently contains information about the location of image files. (See the example that follows the procedure.) CCutil will fail when you have an image directory reference in both the <literal>master.adoc</literal> file and the image tag. Remove the directory path information in the image tag.</simpara>
<orderedlist numeration="arabic">
<title>Procedure</title>
<listitem>
<simpara>Delete the directory path from the image tag.</simpara>
<screen>image::images/prod-renewal.png[]
       ^^^^^^^</screen>
<formalpara>
<title>Result</title>
<para>
<screen>image::prod-renewal.png[]</screen>
</para>
</formalpara>
<tip>
<simpara>You can use find-and-replace to search for <literal>images/</literal> and replace it with nothing.</simpara>
</tip>
</listitem>
</orderedlist>
<formalpara>
<title>Example image directory reference in a <literal>master.adoc</literal> file</title>
<para>In this example, CCutil expects images to be located in the images directory directly beneath the directory where the <literal>master.adoc</literal>  file is located. If the image tag also contains a directory path, CCutil will fail.</para>
</formalpara>
<screen>:imagesdir: images
:vernum: 1
:doctype: book
:toc: left
:toclevels: 3</screen>
</section>
<section xml:id="cleaning_up_variable_references_in_a_topic" remap="_cleaning_up_variable_references_in_a_topic">
<title>Cleaning up variable references in a topic</title>
<simpara>Variable references are not converted by the <literal>doctoascii</literal> utility and must be manually converted.
An XML / DocBook variable is defined by an ampersand (<literal>&amp;</literal>) and semicolon (<literal>;</literal>).  By comparison, variables in AsciiDoc are enclosed in braces <literal>{ }</literal>.</simpara>
<simpara>For example, DocBook might declare a variable entity for “Red Hat Subscription Manager” in a <literal>Migrating.ent</literal> file as follows:</simpara>
<screen>	&lt;!ENTITY RHSM "Red Hat Subscription Manager"&gt;</screen>
<simpara>In the DocBook topic file, the variable RHSM is used like this:</simpara>
<screen>	Locally, &amp;RHSM; tracks what products are actually installed.</screen>
<simpara>The variable <literal>&amp;RHSM</literal> is not changed by the <literal>doctoascii</literal> utility. Instead the variable appears in the output as <literal>&amp;RHSM</literal>. You must convert it to AsciiDoc syntax.</simpara>
<orderedlist numeration="arabic">
<title>Procedure</title>
<listitem>
<simpara>Within your topic file, find all XML variable tags. You can search on the ampersand <literal>&amp;</literal> or semicolon <literal>;</literal> character.</simpara>
</listitem>
<listitem>
<simpara>When you find an XML-tagged variable, change it to AsciiDoc syntax by replacing the <literal>&amp;</literal> and <literal>;</literal> characters with matching braces.</simpara>
<simpara><emphasis role="strong">Variable example (incorrect for AsciiDoc)</emphasis></simpara>
<screen>&amp;RHSM;</screen>
<simpara><emphasis role="strong">Variable example (correct for AsciiDoc)</emphasis></simpara>
<screen>{RHSM}</screen>
</listitem>
</orderedlist>
</section>
<section xml:id="verifying_that_asciidoc_variables_are_defined" remap="_verifying_that_asciidoc_variables_are_defined">
<title>Verifying that AsciiDoc variables are defined</title>
<simpara>AsciiDoc variables are defined in the <literal>attributes.adoc</literal> file. This file is usually located in the same directory (or folder) where the <literal>master.adoc</literal> file for the book is located.</simpara>
<formalpara>
<title>Example of Red Hat Subscription Management variable definition</title>
<para>This example shows partial content of the <literal>attributes.adoc</literal> file.</para>
</formalpara>
<screen>//Red Hat defined with the required non-breaking space
:RH: Red{nbsp}Hat
// Product name variable RHSM defined
:RHSM: {RH} Subscription Management</screen>
<simpara>The company name, Red Hat, is defined separately and then included in the Red Hat Subscription Management variable. You can use the variable <literal>{RHSM}</literal> in your topic rather than typing out <literal>Red Hat Subscription Management</literal> at every mention.</simpara>
<section xml:id="troubleshooting_when_variables_don_t_expand" remap="_troubleshooting_when_variables_don_t_expand">
<title>Troubleshooting when variables don’t expand</title>
<simpara>The <literal>master.adoc</literal> file must include every file that is part of your book. If your variables do not expand when you run them through the CCutil, make sure to include the <literal>attributes.adoc</literal> file in the <literal>master.adoc</literal> file.</simpara>
<formalpara>
<title>Example of the include statement in the <literal>master.adoc</literal> file</title>
<para>
<screen>	include::attributes.adoc[]</screen>
</para>
</formalpara>
</section>
<section xml:id="troubleshooting_when_changes_don_t_appear_in_the_output_after_you_modified_the_input_file" remap="_troubleshooting_when_changes_don_t_appear_in_the_output_after_you_modified_the_input_file">
<title>Troubleshooting when changes don’t appear in the output after you modified the input file</title>
<simpara>If your changes don’t appear in the output file, make sure you saved the input file before running CCutil. Changes are not effective until the file is saved.</simpara>
</section>
</section>
<section xml:id="adjusting_heading_levels_in_the_literal_master_adoc_literal_file" remap="_adjusting_heading_levels_in_the_literal_master_adoc_literal_file">
<title>Adjusting heading levels in the <literal>master.adoc</literal> file</title>
<simpara>You can adjust the heading levels of an included file by adding an offset in the <literal>master.adoc</literal> file include statement. If the heading level is not what you expect, look for extra brackets in the include statement.</simpara>
<formalpara>
<title>Level offset+1 (incorrect)</title>
<para>
<screen>  include::modules/Reporting_Intro.adoc[][leveloffset=+1]
                                       ^^</screen>
</para>
</formalpara>
<formalpara>
<title>Level offset+1 (correct)</title>
<para>
<screen>  include::modules/Reporting_Intro.adoc[leveloffset=+1]</screen>
</para>
</formalpara>
</section>
</chapter>
</book>